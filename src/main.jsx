import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App.jsx';

// const router = createBrowserRouter([
//   {
//     path: '/',
//     element: <App/>,
//   },
//   {
//     path: '/form',
//     element: <Form/>,
//   },
//   {
//     path: '/Table',
//     element: <Table/>,
//   },
// ]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    {/* <RouterProvider router={router} /> */}
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
);
