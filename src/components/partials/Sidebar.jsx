import React from 'react';
import { Link } from 'react-router-dom';

export default function Sidebar() {
  return (
    <div className="vertical-menu">
      <div data-simplebar className="h-100">
        {/* Sidemenu */}
        <div id="sidebar-menu">
          {/* Left Menu Start */}
          <ul className="metismenu list-unstyled" id="side-menu">
            <li className="menu-title" key="t-menu">
              Menu
            </li>

            <li>
              <Link to={`/`} className="waves-effect">
                <i className="bx bx-home-circle"></i>
                <span className="badge rounded-pill bg-info float-end">04</span>
                <span key="t-dashboards">Dashboards</span>
              </Link>
            </li>
            <li>
              <Link to={`/form`} className="waves-effect">
                <i className="bx bxs-eraser"></i>
                <span className="badge rounded-pill bg-info float-end">04</span>
                <span key="t-dashboards">Form</span>
              </Link>
            </li>
            <li>
              <Link to={`/table`} className="waves-effect">
                <i className="bx bx-table "></i>
                <span className="badge rounded-pill bg-info float-end">04</span>
                <span key="t-dashboards">Table</span>
              </Link>
            </li>
          </ul>
        </div>
        {/* Sidebar */}
      </div>
    </div>
  );
}
