import React from 'react';

export default function PageTitle({ text }) {
  return (
    <div className="row">
      <div className="col-12">
        <div className="page-title-box d-sm-flex align-items-center justify-content-between">
          <h4 className="mb-sm-0 font-size-18">{text}</h4>

          <div className="page-title-right">
            <ol className="breadcrumb m-0">
              <li className="breadcrumb-item">
                <a href="!#">
                  <i className="bx bx-home-circle"></i>
                </a>
              </li>
              <li
                className="breadcrumb-item active"
                style={{ textTransform: 'capitalize' }}
              >
                {text}
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  );
}
