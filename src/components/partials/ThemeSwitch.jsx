import React, { useEffect, useState } from 'react';

export default function ThemeSwitch() {
  const isBrowserDefaulDark = () => window.matchMedia('(prefers-color-scheme: dark)').matches;

  const getDefaultTheme = () => {
    const localStorageTheme = localStorage.getItem('theme');
    const browserDefault = isBrowserDefaulDark() ? 'dark' : '';
    return localStorageTheme || browserDefault;
  };
  // const [theme, setTheme] = useState(localStorage.getItem('theme') || '');
  const [theme, setTheme] = useState(getDefaultTheme());
  const toggleTheme = () => {
    if (theme === '') {
      setTheme('dark');
    } else {
      setTheme('');
    }
  };

  useEffect(() => {
    localStorage.setItem('theme', theme);
    document.body.setAttribute('data-layout-mode', theme);
  }, [theme]);

  return (
    <div className="dropdown d-none d-lg-inline-block ms-1">
      <button
        type="button"
        className="btn header-item noti-icon waves-effect"
        onClick={toggleTheme}
      >
        <i className={`bx bxs-${theme === 'dark' ? 'sun' : 'moon'}`}></i>
      </button>
    </div>
  );
}
