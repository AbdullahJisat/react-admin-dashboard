import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Demo from './Demo';
import Form from './Form';
import Table from './Table';
import PageTitle from './partials/PageTitle';

export default function AppRouter() {
  return (
    <Routes>
      <Route
        index
        element={<Demo pageTitle={<PageTitle text="demo" />} />}
        exact
      />
      <Route
        path="/form"
        element={<Form pageTitle={<PageTitle text="form" />} />}
      />
      <Route
        path="/table"
        element={<Table pageTitle={<PageTitle text="table" />} />}
      />
    </Routes>
  );
}
