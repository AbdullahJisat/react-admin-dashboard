import React from 'react';

export default function Form({ pageTitle }) {
  return (
    <>
      {/* page title start */}
      {pageTitle}
      {/* page title end */}
      <div className="row">
        <div className="col-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Textual inputs</h4>
              <p className="card-title-desc">
                Here are examples of <code>.form-control</code> applied to each
                textual HTML5 <code>&lt;input&gt;</code> <code>type</code>.
              </p>

              <div className="mb-3 row">
                <label
                  htmlFor="example-text-input"
                  className="col-md-2 col-form-label"
                >
                  Text
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="text"
                    value="Artisanal kale"
                    id="example-text-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-search-input"
                  className="col-md-2 col-form-label"
                >
                  Search
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="search"
                    value="How do I shoot web"
                    id="example-search-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-email-input"
                  className="col-md-2 col-form-label"
                >
                  Email
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="email"
                    value="bootstrap@example.com"
                    placeholder="Enter Email"
                    id="example-email-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-url-input"
                  className="col-md-2 col-form-label"
                >
                  URL
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="url"
                    value="https:getbootstrap.com"
                    placeholder="Enter URL"
                    id="example-url-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-tel-input"
                  className="col-md-2 col-form-label"
                >
                  Telephone
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="tel"
                    value="1-(555)-555-5555"
                    placeholder="Enter Telephone"
                    id="example-tel-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-password-input"
                  className="col-md-2 col-form-label"
                >
                  Password
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="password"
                    value="hunter2"
                    placeholder="Enter Password"
                    id="example-password-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-number-input"
                  className="col-md-2 col-form-label"
                >
                  Number
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="number"
                    value="42"
                    placeholder="Enter Number"
                    id="example-number-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-datetime-local-input"
                  className="col-md-2 col-form-label"
                >
                  Date and Time
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="datetime-local"
                    value="2019-08-19T13:45:00"
                    id="example-datetime-local-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-date-input"
                  className="col-md-2 col-form-label"
                >
                  Date
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="date"
                    value="2019-08-19"
                    id="example-date-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-month-input"
                  className="col-md-2 col-form-label"
                >
                  Month
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="month"
                    value="2019-08"
                    id="example-month-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-week-input"
                  className="col-md-2 col-form-label"
                >
                  Week
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="week"
                    value="2019-W33"
                    id="example-week-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-time-input"
                  className="col-md-2 col-form-label"
                >
                  Time
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control"
                    type="time"
                    value="13:45:00"
                    id="example-time-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="example-color-input"
                  className="col-md-2 col-form-label"
                >
                  Color
                </label>
                <div className="col-md-10">
                  <input
                    className="form-control form-control-color mw-100"
                    type="color"
                    value="#556ee6"
                    id="example-color-input"
                  />
                </div>
              </div>
              <div className="mb-3 row">
                <label className="col-md-2 col-form-label">Select</label>
                <div className="col-md-10">
                  <select className="form-select">
                    <option>Select</option>
                    <option>Large select</option>
                    <option>Small select</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* end row */}

      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Sizing</h4>
              <p className="card-title-desc">
                Set heights using classNamees like <code>.form-control-lg</code>
                and <code>.form-control-sm</code>.
              </p>
              <div>
                <div className="row">
                  <div className="col-lg-6">
                    <div>
                      <label className="form-label">Default input</label>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="Default input"
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-6">
                    <div className="mt-4">
                      <label className="form-label">Small Input</label>
                      <input
                        className="form-control form-control-sm"
                        type="text"
                        placeholder=".form-control-sm"
                      />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="mt-4">
                      <label className="form-label">Large Input</label>
                      <input
                        className="form-control form-control-lg"
                        type="text"
                        placeholder=".form-control-lg"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* end row */}

      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Range Inputs</h4>
              <p className="card-title-desc">
                Create custom <code>&lt;input type="range"&gt;</code>
                controls with <code>.form-range</code>.
              </p>

              <div className="row">
                <div className="col-lg-6">
                  <div>
                    <label htmlFor="customRange1" className="form-label">
                      Example Range
                    </label>
                    <input
                      type="range"
                      className="form-range"
                      id="customRange1"
                    />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div>
                    <label htmlFor="disabledRange" className="form-label">
                      Disabled Eange
                    </label>
                    <input
                      type="range"
                      className="form-range"
                      id="disabledRange"
                      disabled
                    />
                  </div>
                </div>
              </div>

              <div className="row mt-2">
                <div className="col-lg-6">
                  <div className="mt-4">
                    <h5 className="font-size-14">Min and max</h5>
                    <p className="card-title-desc">
                      Range inputs have implicit values for min and max—0 and
                      100, respectively.
                    </p>
                    <input
                      type="range"
                      className="form-range"
                      min="0"
                      max="5"
                      id="customRange2"
                    />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="mt-4">
                    <h5 className="font-size-14">Steps</h5>
                    <p className="card-title-desc">
                      By default, range inputs “snap” to integer values. To
                      change this, you can specify a <code>step</code> value.
                    </p>
                    <input
                      type="range"
                      className="form-range"
                      min="0"
                      max="5"
                      id="customRange2"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* end row */}

      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Checkboxes</h4>

              <div className="row">
                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14 mb-4">
                      <i className="mdi mdi-arrow-right text-primary me-1"></i>{' '}
                      Form Checkboxes
                    </h5>
                    <div className="form-check mb-3">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="formCheck1"
                      />
                      <label className="form-check-label" htmlFor="formCheck1">
                        Form Checkbox
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="formCheck2"
                        checked
                      />
                      <label className="form-check-label" htmlFor="formCheck2">
                        Form Checkbox checked
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14 mb-4">
                      <i className="mdi mdi-arrow-right text-primary me-1"></i>{' '}
                      Form Checkboxes Right
                    </h5>
                    <div>
                      <div className="form-check form-check-right mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="formCheckRight1"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formCheckRight1"
                        >
                          Form Checkbox Right
                        </label>
                      </div>
                    </div>
                    <div>
                      <div className="form-check form-check-right">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="formCheckRight2"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formCheckRight2"
                        >
                          Form Checkbox Right checked
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14">
                      <i className="mdi mdi-arrow-right text-primary me-1"></i>{' '}
                      Form Checkboxes colors
                    </h5>
                    <p className="sub-header mb-4">
                      Add className <code>.form-check-* </code> for a color
                      Checkboxes
                    </p>

                    <div>
                      <div className="form-check form-check-primary mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="formCheckcolor1"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formCheckcolor1"
                        >
                          Checkbox Primary
                        </label>
                      </div>

                      <div className="form-check form-check-success mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="formCheckcolor2"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formCheckcolor2"
                        >
                          Checkbox Success
                        </label>
                      </div>

                      <div className="form-check form-check-info mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="formCheckcolor3"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formCheckcolor3"
                        >
                          Checkbox Info
                        </label>
                      </div>

                      <div className="form-check form-check-warning mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="formCheckcolor4"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formCheckcolor4"
                        >
                          Checkbox Warning
                        </label>
                      </div>

                      <div className="form-check form-check-danger">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="formCheckcolor5"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formCheckcolor5"
                        >
                          Checkbox Danger
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14">
                      <i className="mdi mdi-arrow-right text-primary me-1"></i>{' '}
                      Form Checkboxes Outline
                    </h5>
                    <p className="sub-header mb-4">
                      Add className <code>.form-checkbox-outline</code> &
                      <code>.form-check-* </code> for a color Checkboxes
                    </p>

                    <div>
                      <div className="form-check form-checkbox-outline form-check-primary mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="customCheckcolor1"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="customCheckcolor1"
                        >
                          Checkbox Outline Primary
                        </label>
                      </div>

                      <div className="form-check form-checkbox-outline form-check-success mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="customCheckcolor2"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="customCheckcolor2"
                        >
                          Checkbox Outline Success
                        </label>
                      </div>

                      <div className="form-check form-checkbox-outline form-check-info mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="customCheckcolor3"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="customCheckcolor3"
                        >
                          Checkbox Outline Info
                        </label>
                      </div>

                      <div className="form-check form-checkbox-outline form-check-warning mb-3">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="customCheckcolor4"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="customCheckcolor4"
                        >
                          Checkbox Outline Warning
                        </label>
                      </div>

                      <div className="form-check form-checkbox-outline form-check-danger">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="customCheckcolor5"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="customCheckcolor5"
                        >
                          Checkbox Outline Danger
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* end row */}

      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Radios</h4>

              <div className="row">
                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14 mb-4">Form Radios</h5>
                    <div className="form-check mb-3">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="formRadios"
                        id="formRadios1"
                        checked
                      />
                      <label className="form-check-label" htmlFor="formRadios1">
                        Form Radio
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="formRadios"
                        id="formRadios2"
                      />
                      <label className="form-check-label" htmlFor="formRadios2">
                        Form Radio checked
                      </label>
                    </div>
                  </div>
                </div>

                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14 mb-4">Form Radios Right</h5>
                    <div>
                      <div className="form-check form-check-right mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadiosRight"
                          id="formRadiosRight1"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadiosRight1"
                        >
                          Form Radio Right
                        </label>
                      </div>
                    </div>

                    <div>
                      <div className="form-check form-check-right">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadiosRight"
                          id="formRadiosRight2"
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadiosRight2"
                        >
                          Form Radio Right checked
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14">
                      <i className="mdi mdi-arrow-right text-primary me-1"></i>{' '}
                      Form Radio colors
                    </h5>
                    <p className="sub-header mb-4">
                      Add className <code>.form-radio-* </code> for a color
                      Radios
                    </p>

                    <div>
                      <div className="form-check form-radio-primary mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadioColor1"
                          id="formRadioColor1"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadioColor1"
                        >
                          Radio Primary
                        </label>
                      </div>

                      <div className="form-check form-radio-success mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadioColor2"
                          id="formRadioColor2"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadioColor2"
                        >
                          Radio Success
                        </label>
                      </div>

                      <div className="form-check form-radio-info mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadioColor3"
                          id="formRadioColor3"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadioColor3"
                        >
                          Radio Info
                        </label>
                      </div>

                      <div className="form-check form-radio-warning mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadioColor4"
                          id="formRadioColor4"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadioColor4"
                        >
                          Radio warning
                        </label>
                      </div>

                      <div className="form-check form-radio-danger mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadioColor5"
                          id="formRadioColor5"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadioColor5"
                        >
                          Radio Danger
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-xl-3 col-sm-6">
                  <div className="mt-4">
                    <h5 className="font-size-14">
                      <i className="mdi mdi-arrow-right text-primary me-1"></i>{' '}
                      Form Radio Outline
                    </h5>
                    <p className="sub-header mb-4">
                      Add className <code>form-radio-outline</code> &
                      <code>.form-radio-* </code> for a color Checkboxes
                    </p>

                    <div>
                      <div className="form-check form-radio-outline form-radio-primary mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadio1"
                          id="formRadio1"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadio1"
                        >
                          Radio Outline Primary
                        </label>
                      </div>
                      <div className="form-check form-radio-outline form-radio-success mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadio2"
                          id="formRadio2"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadio2"
                        >
                          Radio Outline Success
                        </label>
                      </div>
                      <div className="form-check form-radio-outline form-radio-info mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadio3"
                          id="formRadio3"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadio3"
                        >
                          Radio Outline Info
                        </label>
                      </div>
                      <div className="form-check form-radio-outline form-radio-warning mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadio4"
                          id="formRadio4"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadio4"
                        >
                          Radio Outline Warning
                        </label>
                      </div>
                      <div className="form-check form-radio-outline form-radio-danger mb-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="formRadio5"
                          id="formRadio5"
                          checked
                        />
                        <label
                          className="form-check-label"
                          htmlFor="formRadio5"
                        >
                          Radio Outline Danger
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* end row */}

      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Switches</h4>
              <p className="card-title-desc">
                A switch has the markup of a custom checkbox but uses the{' '}
                <code>.form-switch</code> className to render a toggle switch.
                Switches also support the <code>disabled</code> attribute.
              </p>

              <div className="row">
                <div className="col-sm-6">
                  <div>
                    <h5 className="font-size-14 mb-3">Switch examples</h5>

                    <div className="form-check form-switch mb-3">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="flexSwitchCheckDefault"
                      />
                      <label
                        className="form-check-label"
                        htmlFor="flexSwitchCheckDefault"
                      >
                        Default switch checkbox input
                      </label>
                    </div>
                    <div className="form-check form-switch mb-3">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="flexSwitchCheckChecked"
                        checked
                      />
                      <label
                        className="form-check-label"
                        htmlFor="flexSwitchCheckChecked"
                      >
                        Checked switch checkbox input
                      </label>
                    </div>
                    <div className="form-check form-switch mb-3">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="flexSwitchCheckDisabled"
                        disabled
                      />
                      <label
                        className="form-check-label"
                        htmlFor="flexSwitchCheckDisabled"
                      >
                        Disabled switch checkbox input
                      </label>
                    </div>
                    <div className="form-check form-switch">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="flexSwitchCheckCheckedDisabled"
                        checked
                        disabled
                      />
                      <label
                        className="form-check-label"
                        htmlFor="flexSwitchCheckCheckedDisabled"
                      >
                        Disabled checked switch checkbox input
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="mt-4 mt-sm-0">
                    <h5 className="font-size-14 mb-3">Switch sizes</h5>

                    <div className="form-check form-switch mb-3" dir="ltr">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="SwitchCheckSizesm"
                        checked
                      />
                      <label
                        className="form-check-label"
                        htmlFor="SwitchCheckSizesm"
                      >
                        Small Size Switch
                      </label>
                    </div>

                    <div
                      className="form-check form-switch form-switch-md mb-3"
                      dir="ltr"
                    >
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="SwitchCheckSizemd"
                      />
                      <label
                        className="form-check-label"
                        htmlFor="SwitchCheckSizemd"
                      >
                        Medium Size Switch
                      </label>
                    </div>

                    <div
                      className="form-check form-switch form-switch-lg mb-3"
                      dir="ltr"
                    >
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id="SwitchCheckSizelg"
                        checked
                      />
                      <label
                        className="form-check-label"
                        htmlFor="SwitchCheckSizelg"
                      >
                        Large Size Switch
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* end col */}
      </div>
      {/* end row */}

      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title mb-4">File browser</h4>

              <div>
                <h5 className="font-size-14">
                  <i className="mdi mdi-arrow-right text-primary"></i> Default
                  file input
                </h5>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="mt-3">
                      <label htmlFor="formFile" className="form-label">
                        Default file input example
                      </label>
                      <input
                        className="form-control"
                        type="file"
                        id="formFile"
                      />
                    </div>
                  </div>
                </div>
                {/* end row */}

                <div className="row">
                  <div className="col-sm-6">
                    <div className="mt-4">
                      <div>
                        <label htmlFor="formFileSm" className="form-label">
                          Small file input example
                        </label>
                        <input
                          className="form-control form-control-sm"
                          id="formFileSm"
                          type="file"
                        />
                      </div>
                    </div>
                  </div>
                  {/* end col */}
                  <div className="col-sm-6">
                    <div className="mt-4">
                      <div>
                        <label htmlFor="formFileLg" className="form-label">
                          Large file input example
                        </label>
                        <input
                          className="form-control form-control-lg"
                          id="formFileLg"
                          type="file"
                        />
                      </div>
                    </div>
                  </div>
                  {/* end col */}
                </div>
                {/* end row */}
              </div>

              <div className="mt-4 pt-2">
                <h5 className="font-size-14 mb-0">
                  <i className="mdi mdi-arrow-right text-primary"></i> Custom
                  file input
                </h5>
              </div>

              <div className="row">
                <div className="col-sm-6">
                  <div className="mt-4">
                    <div>
                      <label className="form-label">With Label</label>
                      <div className="input-group mb-3">
                        <label
                          className="input-group-text"
                          htmlFor="inputGroupFile01"
                        >
                          Upload
                        </label>
                        <input
                          type="file"
                          className="form-control"
                          id="inputGroupFile01"
                        />
                      </div>
                      <div className="input-group">
                        <input
                          type="file"
                          className="form-control"
                          id="inputGroupFile02"
                        />
                        <label
                          className="input-group-text"
                          htmlFor="inputGroupFile02"
                        >
                          Upload
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-6">
                  <div className="mt-4">
                    <div>
                      <label className="form-label">With Button</label>
                      <div className="input-group mb-3">
                        <button
                          className="btn btn-primary"
                          type="button"
                          id="inputGroupFileAddon03"
                        >
                          Button
                        </button>
                        <input
                          type="file"
                          className="form-control"
                          id="inputGroupFile03"
                          aria-describedby="inputGroupFileAddon03"
                          aria-label="Upload"
                        />
                      </div>

                      <div className="input-group">
                        <input
                          type="file"
                          className="form-control"
                          id="inputGroupFile04"
                          aria-describedby="inputGroupFileAddon04"
                          aria-label="Upload"
                        />
                        <button
                          className="btn btn-primary"
                          type="button"
                          id="inputGroupFileAddon04"
                        >
                          Button
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
