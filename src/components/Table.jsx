import React from 'react';
import Modal from './Modal';

export default function Table({ pageTitle }) {
  return (
    <>
      {/* page title start */}
      {pageTitle}
      {/* page title end */}
      <div className="row">
        <div className="col-xl-12">
          <div className="card">
            <div className="card-header d-flex justify-content-between px-5 bg-white">
              <form>
                <div className="position-relative">
                  <input
                    type="text"
                    className="form-control"
                    style={{
                      border: 'none',
                      height: '38px',
                      paddingLeft: '40px',
                      paddingRight: '20px',
                      backgroundColor: '#f3f3f9',
                      boxShadow: 'none',
                      borderRadius: '30px',
                    }}
                    placeholder="Search..."
                  />
                  <span
                    className="bx bx-search-alt"
                    style={{
                      position: 'absolute',
                      zIndex: '10',
                      fontSize: '16px',
                      lineHeight: '38px',
                      left: '13px',
                      top: '0',
                      color: '#74788d',
                    }}
                  ></span>
                </div>
              </form>
              <div>
                <button
                  type="button"
                  className="btn btn-primary waves-effect waves-light"
                  data-bs-toggle="modal"
                  data-bs-target="#myModal"
                >
                  <i className="bx bx-plus font-size-16 align-middle me-2"></i>{' '}
                  Add New
                </button>
              </div>
            </div>
            <div className="card-body">
              <h4 className="card-title">Striped rows</h4>
              <p className="card-title-desc">
                Use <code>.table-striped</code> to add zebra-striping to any
                table row within the <code>&lt;tbody&gt;</code>.
              </p>

              <div className="table-responsive">
                <table className="table table-striped mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Username</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Mark</td>
                      <td>Otto</td>
                      <td>@mdo</td>
                    </tr>
                    <tr>
                      <th scope="row">2</th>
                      <td>Jacob</td>
                      <td>Thornton</td>
                      <td>@fat</td>
                    </tr>
                    <tr>
                      <th scope="row">3</th>
                      <td>Larry</td>
                      <td>the Bird</td>
                      <td>@twitter</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* modal */}
      <Modal />
      {/* modal end */}
    </>
  );
}
