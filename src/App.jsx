import React, { Suspense, lazy } from 'react';
// import AppRouter from './components/AppRouter';
import Footer from './components/partials/Footer';
import Header from './components/partials/Header';
import Sidebar from './components/partials/Sidebar';
const AppRouter = lazy(() => import('./components/AppRouter'));
const loaderBg = {
  position: 'fixed',
  top: '0',
  bottom: '0',
  left: '0',
  right: '0',
  backgroundColor: '#fff',
  zIndex: '9999',
};

const loaderBar = {
  position: 'fixed',
  height: '5px',
  width: '80%',
  top: '0',
  left: '0',
  background: '#2196f3',
  animation: 'barfiller 2s',
};
function App() {
  return (
    // <>
    //   <div>
    //     <a href="https://vitejs.dev" target="_blank">
    //       <img src={viteLogo} className="logo" alt="Vite logo" />
    //     </a>
    //     <a href="https://react.dev" target="_blank">
    //       <img src={reactLogo} className="logo react" alt="React logo" />
    //     </a>
    //   </div>
    //   <h1>Vite + React</h1>
    //   <div className="card">
    //     <button onClick={() => setCount((count) => count + 1)}>
    //       count is {count}
    //     </button>
    //     <p>
    //       Edit <code>src/App.jsx</code> and save to test HMR
    //     </p>
    //   </div>
    //   <p className="read-the-docs">
    //     Click on the Vite and React logos to learn more
    //   </p>
    // </>
    <>
      {/* Begin page */}
      <div id="layout-wrapper">
        {/* header start */}
        <Header />
        {/* header end */}
        {/* sidebar start */}
        <Sidebar />
        {/* sidebar end */}

        {/* start main content */}
        <div className="main-content">
          <div className="page-content">
            <div className="container-fluid">
              <Suspense
                fallback={
                  <div style={loaderBg}>
                    <div style={loaderBar}></div>
                  </div>
                }
              >
                <AppRouter />
              </Suspense>
            </div>
            {/* container-fluid */}
          </div>
          {/* End Page-content */}

          {/* footer start */}
          <Footer />
          {/* footer end */}
        </div>
        {/* end main content */}
      </div>
      {/* END layout-wrapper */}
    </>
  );
}

export default App;
